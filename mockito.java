//@RunWith(MockitoJUnitRunner.class) - replaced with .initMocks()
public class ResponseMapperTest {

    @InjectMocks
    private Mapper mapper = new Mapper();
    
    @Mock
    private CommonMapper commonMapper;

    @Before
    public void init() {
         // instead of @RunWith use below:
        MockitoAnnotations.initMocks(this);

        Mockito.when(commonMapper.getDuration(Mockito.any())).thenReturn(Duration.ZERO);
    }

    @Test
    public void test1() {
        assertTrue(mapper.getBla())
    }
}