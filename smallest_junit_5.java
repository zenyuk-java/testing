package net.flitech.land.hotel.location.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

class Bla {
  public String say(String s) {
    return "bla" + s;
  }
}

@ExtendWith(MockitoExtension.class)
class BlaTest {
  private Bla bla = new Bla();

  @Test
  public void testSay() {

    Assertions.assertEquals(bla.say("1"), "bla1");
  }
}